# Flywheel Shooter

![coverage](https://gitlab.com/chargerrobotics3786/common/templates/flywheel-shooter/badges/main/coverage.svg?job=report-coverage)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chargerrobotics3786_flywheel-shooter&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=chargerrobotics3786_flywheel-shooter)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=chargerrobotics3786_flywheel-shooter&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=chargerrobotics3786_flywheel-shooter)

<!-- Remove this section after creating a library from this template -->
This template provides a jumping off point for creating a new Flywheel Shooter that complies with the [Reusable Library Design](https://chargerrobotics.atlassian.net/wiki/spaces/FRC24/pages/54820866/Software+Reusable+Library+Design).

## Important Versions

This library uses the below versions for key components:

- Java 17
- Gradle 8.10.2

## Getting started

<!-- Remove this section after creating a library from this template -->
Address all of the following issues that come with the template in order.

- Lines 149-155 of `lib/build.gradle` need to be updated with the appropriate values and the comments removed.
- This `README.md` needs to have the main header renamed and the badges updated to the correct urls. Additionally the code snippet at the end shall be updated to update the `implementation` to point to the correct maven package path.
- The `rootProject.name` in `settings.gradle` must be updated to the correct java package that the library will be packaged in.
- The `sonar.projectKey` and `sonar.projectName` fields need to be updated in `lib/build.gradle`
- The `projectKey` needs to be updated in `.sonarlint/connectedMode.json`

## Using This Library

### Gradle

```gradle
repositories {
    maven {
        url "https://gitlab.com/api/v4/groups/chargerrobotics3786/common/-/packages/maven"
        name "Charger Robotics Common"
    }
}

dependencies {
    implementation "com.chargerrobotics.common.subsystems:flywheel-shooter:<version>"
}
```
